# IS Movie DB Application
## Setup Instructions
The application has been built on Laravel as a Framework. 
Please make sure that you have the environment setup to run Laravel, if not, you can follow these quick setup tools:
1.	Install Composer on your machine, visit https://getcomposer.org/
2.	Once you have Composer installed, navigate into the application’s directory(in you can then copy the repo from BitBucket via  or download the zip file from Google Drive via - )
a.	Run composer update from the application’s directory via terminal
b.	Then php artisan serve to get the Laravel server up and running, also via terminal, and you will see the application start from http://127.0.0.1:8000 - just make sure that you don’t have other applications running from the same port
i.	If you are running the application via MAMP/XAMPP/LAMP then you will need to navigate into the application directory via your browser then the /public directory

Right, now you have the application running, you can work through it. The configuration details(database, API keys, etc) are saved in the application directory in the README.md file

## Extended Details
### Database
DB_HOST: 164.160.91.12
DB_PORT: 3306
DB_DATABASE: prologiq_ismovieapp
DB_USERNAME: prologiq_blackdev
DB_PASSWORD: @PrologiQ2906

### OMDBAPI
Key: 3013aae