
$apiKey = '3013aae';

var randomMovieArray = ['Avengers', 'Black Panther', 'The Mist', 'Lord of the rings', 'Star Wars: The Rise of Skywalker', 'Running Man', 'Planet of the apes', 'Harry Potter'];

var randomNumber = Math.floor((Math.random() * randomMovieArray.length - 1 ) + 1);

var randomMovie = randomMovieArray[randomNumber];


function getApiCall()
{
    $.getJSON('https://www.omdbapi.com/?t=' + encodeURI(randomMovie)+'&apikey='+$apiKey).then(function(response){
        //console.log(response);
        var image = response.Poster;
        var title = response.Title;

        if(image !== "N/A")
        {
            $('img').attr('src', image);

        }
        $('#featuredMovie').html(title);
        console.log(title);



    });
}

getApiCall();

function movieSelected(id)
{
    sessionStorage.setItem('movieId', id);
    window.location = "movie";
    return false;
}

function getMovie()
{
    let movieId = sessionStorage.getItem('movieId');

    axios.get('https://www.omdbapi.com/?i='+ searchText+'&apikey='+$apiKey)
    .then((response) => {
        console.log(response);

        let movie = response.data;
        let output = `
                <div class="row">
                    <div class="col-md-4">
                        <img src="${movie.Poster}" class="thumbnail">
                    </div>
                    <div class="col-md-8">
                        <h2>${movie.Title}</h2>
                        <ul class="list-group">
                            <li class="list-group-item"><strong>Release Year:</strong> ${movie.Year}</li>
                            <li class="list-group-item"><strong>Genre:</strong> ${movie.Genre}</li>
                            <li class="list-group-item"><strong>IMDB Rating:</strong> ${movie.imdbRating}</li>
                            <li class="list-group-item"><strong>Actosr:</strong> ${movie.Actors}</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="well">
                        <h3>Plot</h3>
                        ${movie.Plot}
                        <hr>
                        <a href="http://imdb.com/title/${movie.imdbID}" target="_blank" class="btn btn-primary">View on IMDB</a>
                        <a href="/" class="btn btn-default">Bakc to Search</a>
                    </div>
                </div>
            `;

        $('#movie').html(output);
    })
    .catch((err) => {
        console.log(err);
    });
}


function getMovies(searchText)
{
   axios.get('https://www.omdbapi.com/?s='+ searchText+'&apikey='+$apiKey)
    .then((response) => {
        console.log(response);
        let movies = response.data.Search;
        let output = '';
        $.each(movies, (index, movie) => {
            output += `
                <div class="col md-3">
                    <div class="well text-center">
                        <img src="${movie.Poster}">
                        <h5> ${movie.Title}</h5>

                        <p>
                        ${movie.Plot}
                        <br><br>
                        Release Year: ${movie.Year}
                        <br>
                        IMDB Rating: ${movie.imdbRating}
                        </p>
                        <a onclick="movieSelected('${movie.imdbID}')" class="btn btn-primary" href="#">Movie Details</a>
                    </div>
                </div>
            `;
        });

        $('#movies').html(output);
    })
    .catch((err) => {
        console.log(err);
    });
}

$(document).ready(() =>{
    $('#searchForm').on('submit', (e) =>
    {
        // console.log($('#searchText').val());
        let searchText = $('#searchText').val();
        getMovies(searchText);
        e.preventDefault();

    });

});
