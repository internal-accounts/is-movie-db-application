<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="IS Technical test for Software development department">
    <meta name="author" content="Samuel Phatlane">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom Layout -->
    <link href="css/app_foundation.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <title>The Plot</title>

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-light bg-light static-top">
        <div class="container">
            <a class="navbar-brand" href="{{ route('home') }}">The Movie Library</a>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        |
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div>
    </nav>

    <!-- Masterhead -->


    <div class="container body-content">
        <div class="row">
            <div class="col-xl-9 mx-auto movie-header-title">
                <h1 class="mb-5">Movie API Project</h1>
            </div>

            <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
                <form id="searchForm">
                    <div class="form-row">
                        <div class="col-12 col-md-9 mb-2 mb-md-0">
                            <input type="text" class="form-control form-control-lg" id="searchText" placeholder="Enter you movie/movie id to search">
                        </div>
                        <div class="col-12 col-md-3">
                            <button type="submit" class="btn btn-block btn-lg btn-primary">Go</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

        <div class="breaker-space"></div>

        <div class="row search-result">
            <div class="container">
                <h2 class="dark-text title-result">Featured Movie</h2>
                <h3 class="dark-text title-result" id="featuredMovie">No Movie Found</h3>
                <img src="img/No_Image_Available.png" class="poster-result">
            </div>
        </div>

        <div class="breaker-space"></div>

        <div class="container">
            <div id="movies" class="row"></div>
        </div>




    <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="js/app_foundation.js"></script>
</body>

</html>
